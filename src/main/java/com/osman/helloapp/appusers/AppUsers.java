package com.osman.helloapp.appusers;

import java.util.Collection;
import java.util.Collections;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.servlet.annotation.HandlesTypes;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode
@NoArgsConstructor

public class AppUsers implements UserDetails {
   
    @Id
    @SequenceGenerator(name = "student_sequence", sequenceName = "student_sequence", allocationSize = 1) 
    @GeneratedValue(strategy =  GenerationType.SEQUENCE , generator = "student_sequence")


    

    private Long id;
    private String name;
    private String username;
    private String email;
    private String password;

    @Enumerated(EnumType.STRING)
    private AppUsersRole appusersrole;
    private Boolean locked;
    private Boolean enabled;

    public AppUsers(String name, 
    String username, String email, 
    String password, AppUsersRole appusersrole,
            Boolean locked, Boolean enabled) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.appusersrole = appusersrole;
        this.locked = locked;
        this.enabled = enabled;

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
   
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(appusersrole.name());
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
   
        return password;
    }

    @Override
    public String getUsername() {
   
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
   
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
   
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
   
        return true;
    }

    @Override
    public boolean isEnabled() {
   
        return enabled;
    }

}
